import java.util.Scanner;

public class QIV {
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);// create Scanner Obj
        System.out.print("Enter an integer between 0-999: "); // Prompt the user to enter integer
        int number = sc.nextInt();
        int length_number = String.valueOf(number).length();
        String message = "";

        if(length_number == 1) {
            message = String.format("The number has %d digit. The digit is %d.", length_number, number);

        } else if (length_number == 2) {
            int first_digit = number % 10;
            number /= 10;

            int second_digit = number % 10;

            message = String.format("The number has %d digits. The digits are %d and %d.", length_number, first_digit, second_digit);

        } else if (length_number == 3) {
            int first_digit = number % 10;
            number /= 10;

            int second_digit = number % 10;
            number /= 10;

            int third_digit = number % 10;

            message = String.format("The number has %d digits. The digits are %d, %d, and %d.", length_number, first_digit, second_digit, third_digit);
        }

        System.out.println(message);
    }
}
